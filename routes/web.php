<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('test-laravel','Flightcontroller@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('clients','Flightcontroller@client_list');

Route::get('api-token','Flightcontroller@test_api');
